let unloaded = false;

function unloadHandler() {
  if (!unloaded) {
    doQuit();
  }
}

// const completeBtn = document.querySelector('#completeBtn');
// completeBtn.addEventListener('click', setScoCompleted);

function setScoCompleted() {
  console.log('setScoCompleted')
  doLMSSetValue("cmi.core.lesson_status", "completed");
}

loadPage();
setScoCompleted()

window.onbeforeunload = unloadHandler;
window.onunload = unloadHandler;